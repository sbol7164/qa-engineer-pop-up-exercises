Pre-requisites

- I used my login, but you will need to input your own login details.
- I ran this project using a project in Eclipse that used the Selenium 2.0 libraries and runs in Firefox which must be installed

Assume

- this is for one browser only - equivalents for other browsers to be performed if required
- this is a single creation - assume the creation with the full range of inputs will be tested using extensive unit testing

Issues:

- click() does not always work, appears to need window to stay in focus, requires a click during process