import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Tester {

	public static void main(String[] args) {

        // Create a new instance of the Firefox driver
        WebDriver driver = new FirefoxDriver();

        // Set the initial page reference to the login page
        driver.get("https://jira.atlassian.com/login.jsp?os_destination=%2Fsecure%2FDashboard.jspa");
        
        // Create the login page object
        LoginPage loginPage = new LoginPage(driver);
        
        // Login
        JIRATestPage testPage = loginPage.loginAs("sbol7164@gmail.com", "XXXXXXXX");

        // Perform test and display result
        if (testPage.test()) {
        	System.out.println("Thumbs Up");
        } else {
        	System.out.println("Send it back");
        }        	

	}	

}